package ru.tsc.karbainova.tm.service;

import java.sql.Connection;
import java.util.Collection;
import java.util.List;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.api.repository.IOwnerRepository;
import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.api.repository.IRepository;
import ru.tsc.karbainova.tm.api.repository.ISessionRepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.IService;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.model.AbstractEntity;
import ru.tsc.karbainova.tm.repository.AbstractRepository;
import ru.tsc.karbainova.tm.repository.ProjectRepository;

public abstract class AbstractService<E extends AbstractEntity> implements IService<E> {

    @NonNull
    protected final IConnectionService connectionService;

    protected AbstractService(@NonNull IConnectionService connectionService) {
        this.connectionService = connectionService;
    }

    public abstract IRepository<E> getRepository(@NonNull Connection connection);

    @Override
    @SneakyThrows
    public List<E> findAll() {
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IRepository repository = getRepository(connection);
            return repository.findAll();
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void clear() {
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IRepository repository = getRepository(connection);
            repository.clear();
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(E entity) {
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IRepository repository = getRepository(connection);
            repository.remove(entity);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public E add(E entity) {
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IRepository repository = getRepository(connection);
            repository.add(entity);
            connection.commit();
            return entity;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public boolean exists(String id) {
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IRepository repository = getRepository(connection);
            return repository.exists(id);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public E findById(String id) {
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IRepository<E> repository = getRepository(connection);
            return repository.findById(id);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(String id) {
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IRepository<E> repository = getRepository(connection);
            repository.removeById(id);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(Collection<E> collection) {
        if (collection == null) return;
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IRepository<E> repository = getRepository(connection);
            repository.addAll(collection);
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }
}
