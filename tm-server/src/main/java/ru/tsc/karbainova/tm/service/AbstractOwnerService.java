package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.api.repository.IOwnerRepository;
import ru.tsc.karbainova.tm.api.repository.IRepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.IOwnerService;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.model.AbstractOwnerEntity;

import java.sql.Connection;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public abstract class AbstractOwnerService<E extends AbstractOwnerEntity> extends AbstractService<E> implements IOwnerService<E> {

    @NonNull
    public AbstractOwnerService(@NonNull final IConnectionService connectionService) {
        super(connectionService);
    }

    public abstract IOwnerRepository<E> getRepository(@NonNull Connection connection);

    @Override
    @NonNull
    @SneakyThrows
    public List<E> findAll(String userId) {
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.findAll(userId);
        } finally {
            connection.close();
        }
    }


    @Override
    @SneakyThrows
    public void clear(String userId) {
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IOwnerRepository repository = getRepository(connection);
            repository.clear(userId);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public List<E> findAll(String userId, Comparator<E> comparator) {
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.findAll(userId, comparator);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public E findById(String userId, String id) {
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IOwnerRepository<E> repository = getRepository(connection);
            return repository.findById(userId, id);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeById(String userId, String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyNameException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IOwnerRepository<E> repository = getRepository(connection);
            repository.removeById(userId, id);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void addAll(@NonNull String userId, Collection<E> collection) {
        if (collection == null || collection.isEmpty()) return;
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final IOwnerRepository<E> repository = getRepository(connection);
            repository.addAll(userId, collection);
        } finally {
            connection.close();
        }
    }
}
