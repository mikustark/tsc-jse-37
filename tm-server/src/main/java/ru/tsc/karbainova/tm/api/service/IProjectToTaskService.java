package ru.tsc.karbainova.tm.api.service;

import ru.tsc.karbainova.tm.model.Task;

import java.util.List;

public interface IProjectToTaskService {

    List<Task> findTaskByProjectId(String userId, String projectId);

    Task taskBindById(String userId, String projectId, String taskId);

    Task taskUnbindById(String userId, String projectId, String taskId);

    void removeAllTaskByProjectId(String userId, String projectId);

    void removeById(String userId, String projectId);
}
