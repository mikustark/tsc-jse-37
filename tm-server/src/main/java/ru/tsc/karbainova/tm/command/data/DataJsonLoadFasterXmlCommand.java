package ru.tsc.karbainova.tm.command.data;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.dto.Domain;
import ru.tsc.karbainova.tm.enumerated.Role;

import java.nio.file.Files;
import java.nio.file.Paths;

public class DataJsonLoadFasterXmlCommand extends AbstractDataCommand {
    @Override
    public String name() {
        return "json-faster-load";
    }

    @Override
    public String arg() {
        return null;
    }

    @Override
    public String description() {
        return "Json faster load";
    }

    @SneakyThrows
    @Override
    public void execute() {
        @NonNull final String json = new String(Files.readAllBytes(Paths.get(FILE_FASTERXML_JSON)));
        @NonNull final ObjectMapper objectMapper = new ObjectMapper();
        @NonNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    public @Nullable Role[] roles() {
        return new Role[0];
    }
}
