package ru.tsc.karbainova.tm.api.repository;

import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.model.Project;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;

public interface IProjectRepository extends IOwnerRepository<Project> {

    void add(String userId, Project project);

    void addAll(Collection<Project> tasks);

    void remove(String userId, Project project);

    Project update(Project entity);

    boolean existsById(String userId, String id);

    List<Project> findAll();

    List<Project> findAll(String userId);

    List<Project> findAll(String userId, Comparator<Project> comparator);

//    void clear();
//
//    void clear(String userId);

    Project findById(String userId, String id);

    Project findByIndex(String userId, int index);

    Project findByName(String userId, String name);

    void removeById(String userId, String id);

    void removeByName(String userId, String name);

    void removeByIndex(String userId, int index);


}
