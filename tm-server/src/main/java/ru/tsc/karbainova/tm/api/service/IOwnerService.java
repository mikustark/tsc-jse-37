package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.api.repository.IOwnerRepository;
import ru.tsc.karbainova.tm.model.AbstractOwnerEntity;

import java.util.Collection;
import java.util.List;

public interface IOwnerService<E extends AbstractOwnerEntity> extends IOwnerRepository<E> {

}
