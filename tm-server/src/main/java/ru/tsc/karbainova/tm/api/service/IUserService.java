package ru.tsc.karbainova.tm.api.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserService {

    void addAll(Collection<User> users);

    void clear();

    List<User> findAll();

    User findById(String id);

    User findByLogin(String login);

    User findByEmail(@NonNull String email);

    User create(String login, String password);

    User create(String login, String password, String email);

    User updateUser(
            String userId,
            String firstName,
            String lastName,
            String middleName
    );

    User setPassword(String userId, String password);

}
