package ru.tsc.karbainova.tm.api.repository;

import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.model.User;

import java.util.Collection;
import java.util.List;

public interface IUserRepository extends IRepository<User> {
    List<User> findAll();

    User add(User user);

    void addAll(Collection<User> tasks);

    User findById(String id);

    User update(User entity);

    User findByLogin(String login);

    User findByEmail(String email);

    void removeUser(User user);

    void removeById(String id);

    void removeByLogin(String login);

    void clear();

}
