package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import org.jetbrains.annotations.Nullable;
import ru.tsc.karbainova.tm.api.repository.IProjectRepository;
import ru.tsc.karbainova.tm.api.repository.ITaskRepository;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.ITaskService;
import ru.tsc.karbainova.tm.enumerated.Status;
import ru.tsc.karbainova.tm.exception.empty.EmptyIdException;
import ru.tsc.karbainova.tm.exception.empty.EmptyIndexException;
import ru.tsc.karbainova.tm.exception.empty.EmptyNameException;
import ru.tsc.karbainova.tm.exception.entity.ProjectNotFoundException;
import ru.tsc.karbainova.tm.exception.entity.TaskNotFoundException;
import ru.tsc.karbainova.tm.model.Project;
import ru.tsc.karbainova.tm.model.Task;
import ru.tsc.karbainova.tm.repository.TaskRepository;

import java.sql.Connection;
import java.util.Date;

public class TaskService extends AbstractOwnerService<Task> implements ITaskService {

    public TaskService(IConnectionService connectionService) {
        super(connectionService);
    }

    public ITaskRepository getRepository(@NonNull Connection connection) {
        return new TaskRepository(connection);
    }

    @Override
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) return;
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        final Task task = new Task();
        task.setName(name);
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.add(userId, task);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void create(@NonNull String userId, @NonNull String name, @NonNull String description) {
        if (name == null || name.isEmpty()) return;
        if (userId == null || userId.isEmpty()) return;
        if (description == null || description.isEmpty()) return;
        final Task task = new Task();
        task.setName(name);
        task.setDescription(description);
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.add(userId, task);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void add(@NonNull String userId, @NonNull Task task) {
        if (userId == null || userId.isEmpty()) return;
        if (task == null) throw new TaskNotFoundException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.add(userId, task);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void remove(@NonNull String userId, @NonNull Task task) {
        if (task == null) throw new TaskNotFoundException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.remove(userId, task);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Task updateById(@NonNull String userId, @NonNull String id,
                           @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            Task task = taskRepository.findById(userId, id);
            if (task == null) throw new TaskNotFoundException();
            task.setName(name);
            task.setDescription(description);
            taskRepository.update(task);
            connection.commit();
            return task;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Task updateByIndex(@NonNull String userId, @NonNull Integer index,
                              @NonNull String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            if (index > taskRepository.findAll(userId).size() - 1) throw new EmptyIndexException();
            Task task = taskRepository.findByIndex(userId, index);
            if (task == null) throw new TaskNotFoundException();
            task.setName(name);
            task.setDescription(description);
            taskRepository.update(task);
            connection.commit();
            return task;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByIndex(@NonNull String userId, @NonNull Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();

        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            if (index > taskRepository.findAll(userId).size() - 1) throw new EmptyIndexException();
            taskRepository.removeByIndex(userId, index);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public void removeByName(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            taskRepository.removeByName(userId, name);
            connection.commit();
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Task findByIndex(@NonNull String userId, @NonNull Integer index) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            if (index > taskRepository.findAll(userId).size() - 1) throw new EmptyIndexException();
            return taskRepository.findByIndex(userId, index);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Task findByName(@NonNull String userId, @NonNull String name) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            return taskRepository.findByName(userId, name);
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Task startById(@NonNull String userId, @NonNull String id) {
        if (userId == null || userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            final Task task = taskRepository.findById(userId, id);
            if (task == null) throw new ProjectNotFoundException();
            task.setStatus(Status.IN_PROGRESS);
            connection.commit();
            return task;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Task startByIndex(@NonNull String userId, @NonNull Integer index) {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            if (index > taskRepository.findAll(userId).size() - 1) throw new EmptyIndexException();
            final Task task = taskRepository.findByIndex(userId, index);
            if (task == null) throw new ProjectNotFoundException();
            task.setStatus(Status.IN_PROGRESS);
            connection.commit();
            return task;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Task startByName(@NonNull String userId, @NonNull String name) {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            final Task task = taskRepository.findByName(userId, name);
            if (task == null) throw new ProjectNotFoundException();
            task.setStatus(Status.IN_PROGRESS);
            connection.commit();
            return task;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Task finishById(@NonNull String userId, @NonNull String id) {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (id == null || id.isEmpty()) throw new EmptyIdException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            final Task task = taskRepository.findById(userId, id);
            if (task == null) throw new ProjectNotFoundException();
            task.setStatus(Status.COMPLETE);
            task.setFinishDate(new Date());
            connection.commit();
            return task;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Task finishByIndex(@NonNull String userId, @NonNull Integer index) {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (index == null || index < 0) throw new EmptyIndexException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            if (index > taskRepository.findAll(userId).size() - 1) throw new EmptyIndexException();
            final Task task = taskRepository.findByIndex(userId, index);
            if (task == null) throw new ProjectNotFoundException();
            task.setStatus(Status.COMPLETE);
            task.setFinishDate(new Date());
            connection.commit();
            return task;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }

    @Override
    @SneakyThrows
    public Task finishByName(@NonNull String userId, @NonNull String name) {
        if (userId.isEmpty()) throw new EmptyIdException();
        if (name == null || name.isEmpty()) throw new EmptyNameException();
        @NonNull final Connection connection = connectionService.getConnection();
        try {
            @NonNull final ITaskRepository taskRepository = getRepository(connection);
            final Task task = taskRepository.findByName(userId, name);
            if (task == null) throw new ProjectNotFoundException();
            task.setStatus(Status.COMPLETE);
            task.setFinishDate(new Date());
            connection.commit();
            return task;
        } catch (@NonNull final Exception e) {
            connection.rollback();
            throw e;
        } finally {
            connection.close();
        }
    }
}
