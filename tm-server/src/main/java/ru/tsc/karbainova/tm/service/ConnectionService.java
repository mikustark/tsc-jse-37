package ru.tsc.karbainova.tm.service;

import lombok.NonNull;
import lombok.SneakyThrows;
import ru.tsc.karbainova.tm.api.service.IConnectionService;
import ru.tsc.karbainova.tm.api.service.IPropertyService;

import java.sql.Connection;
import java.sql.DriverManager;

public class ConnectionService implements IConnectionService {

    @NonNull
    private final IPropertyService propertyService;

    public ConnectionService(@NonNull IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Override
    @SneakyThrows
    public Connection getConnection() {
        final String user = propertyService.getJdbcUser();
        final String password = propertyService.getJdbcPassword();
        final String url = propertyService.getJdbcUrl();
        final Connection connection = DriverManager.getConnection(url, user, password);
        connection.setAutoCommit(false);
        return connection;
    }
}
